from __future__ import print_function
import config
import os
import runtests
import setup
import subprocess
from datetime import date, timedelta
from jobread import jsonread


class TestResults:
    """
    Functions to check if the tests in runtests.py work
    """
    def __init__(self, passwd=''):
        self.rt = runtests.AllTests()
        cwd = self.rt.dir
        self.testdir = cwd + '/testfiles/jobjson/'
        self.names = self.getjobnames()
        self.dba = config.setupdb('pysched')
        self.results = []
        self.passwd = passwd
        st = setup.Configure()
        self.cwd = st.setcwd()
        self.dir = self.setdir()

    def setdir(self):
        """
        Sets up the working directory for tests

        Returns:
             working directory
        """
        testdir, filename = os.path.split(os.path.abspath(__file__))
        return testdir

    def getjobnames(self):
        """
        Get the job names from the JSON file

        Returns:
            a list of names
        """
        names = []
        for filename in os.listdir(self.testdir):
            jsonfile = self.testdir + filename
            jr = jsonread.JobConf(jsonfile=jsonfile)
            names.append([i['jobname'] for i in jr.jsonobj])
        jobnames = [n for l in names for n in l]
        return jobnames

    def checkdbtables(self):
        """
        Check to make sure that the tables and therefore database
        have been created. If the db doesn't exist then this check
        will fail the same if one or more tables don't exist

        Returns:
            Either True or False
        """
        query = 'SHOW TABLES from {}'.format(self.rt.dbname)
        expected = ['agentstable',
                    'completed_jobs',
                    'jobconfig',
                    'scheduled_jobs']
        outcome = self.dba.run(query)
        result = all(any(t in i for i in outcome) for t in expected) if outcome else False
        return result

    def checkconfigname(self):
        """
        Check to make sure the jobs have been added to the jobconfig
        table. This does not check if the config is how it has been
        set up in the JSON file. That is handled by checkconfigsettings()

        Returns:
             A list of True and False. One for each job
        """
        resultlist = []
        for n in self.names:
            query = """SELECT JobName FROM {}.{}
            WHERE JobName = '{}'""".format(self.dba.dbname,
                                           self.dba.conftable,
                                           n)
            configcheck = self.dba.run(query)
            result = any(n in i for i in configcheck)
            resultlist.append(result)
        return False if False in resultlist else True

    def jobconfigcheck(self):
        """
        Check that the configuration table matches that set
        in the .JSON file

        Returns:
            List of FAIL and PASS depending on outcome
        """
        resultlist = []
        for filename in os.listdir(self.testdir):
            jobkeys = []
            jsonfile = self.testdir + filename
            jr = jsonread.JobConf(jsonfile=jsonfile)
            job = jr.jsonobj
            for i in job:
                jobkeys.append([k for k in i])
                for j in jobkeys[0]:
                    query = """SELECT {} FROM {}.{}
                                WHERE JobName = '{}'""".format(j,
                                                               self.dba.dbname,
                                                               self.dba.conftable,
                                                               job[0]['jobname'])
                    confs = self.dba.run(query)
                    resultlist.append("PASS" if job[0][j] == confs[0][0] else "FAIL")
        return False if "FAIL" in resultlist else "PASS"

    def checkscheduled(self):
        """
        Check that the jobs were schedule.
        Again, this does not check that the schedule is correct

        Returns:
             List of True and False. One for each job
        """
        resultlist = []
        for n in self.names:
            query = """SELECT COUNT(*) FROM {}.{}
            WHERE JobName = '{}'""".format(self.dba.dbname,
                                           self.dba.schedtable,
                                           n)
            schedcheck = self.dba.run(query)
            resultlist.append("PASS" if int(schedcheck[0][0]) else "FAIL")
        return False if "FAIL" in resultlist else True

    def basequery(self, sel, jobname, extra=''):
        """
        Base query for all the other tests in checkscheduleconfig()

        Args:
            sel: What to select form table
            jobname: Name of the job
            extra: Anything to add to the query e.g. LIMIT x

        Returns:
            The query as a string

        """
        base = """SELECT {} FROM {}.{}
                WHERE JobName = '{}'
                {}""".format(sel,
                             self.dba.dbname,
                             self.dba.schedtable,
                             jobname,
                             extra)
        return base

    def hourly(self):
        """
        Test hourly.json and append pass/fail to results list
        """
        hourlyquery = self.basequery('Time', 'run every 2 hours', 'LIMIT 3')
        hours = self.dba.run(hourlyquery)
        for t in range(0, len(hours) - 1):
            diff = hours[t + 1][0] - hours[t][0]
            self.results.append("PASS" if str(diff) == "2:00:00" else "FAIL")

    def iterinmonth(self):
        """
        Test iterinmonth.json and append pass/fail to results list
        """
        iterquery = self.basequery('Date', 'iterinmonth')
        iters = self.dba.run(iterquery)
        for i in iters:
            self.results.append("PASS" if i[0].isoweekday() == 5 else "FAIL")

    def minutely(self):
        """
        Test minutely.json and append pass/fail to results list
        """
        twentyquery = self.basequery('Time', 'run 20 minutes', 'LIMIT 4')
        twenty = self.dba.run(twentyquery)
        for tw in range(0, len(twenty) - 1):
            tdiff = twenty[tw + 1][0] - twenty[tw][0]
            self.results.append("PASS" if str(tdiff) == "0:20:00" else "FAIL")
        nintyquery = self.basequery('Time', 'every 90 minutes', 'LIMIT 4')
        ninty = self.dba.run(nintyquery)
        for ny in range(0, len(ninty) - 1):
            ndiff = ninty[ny + 1][0] - ninty[ny][0]
            self.results.append("PASS" if str(ndiff) == "1:30:00" else "FAIL")

    def monthly(self):
        """
        Test monthly.json and append pass/fail to results list
        """
        twoquery = self.basequery('COUNT(*)', 'run monthly')
        twomonth = self.dba.run(twoquery)[0][0]
        self.results.append("PASS" if int(twomonth) == 1 else "FAIL")
        onequery = self.basequery('Date', 'run each month')
        onemonth = self.dba.run(onequery)
        self.results.append("PASS" if len(onemonth) == 2 else "FAIL")
        for om in range(0, len(onemonth) - 1):
            omdiff = onemonth[om + 1][0] - onemonth[om][0]
            self.results.append("PASS" if "28 days" in str(omdiff)
                                or "29 days" in str(omdiff)
                                or "30 days" in str(omdiff)
                                or "31 days" in str(omdiff)
                                else "FAIL")

    def repdays(self):
        """
        Test repdays.json and append pass/fail to results list
        """
        daysquery = self.basequery('Date', 'run every 2 days', 'LIMIT 4')
        days = self.dba.run(daysquery)
        for d in range(0, len(days) - 1):
            ddiff = days[d + 1][0] - days[d][0]
            self.results.append("PASS" if "2 days" in str(ddiff) else "FAIL")

    def totaliter(self):
        """
        Test totaliter.json and append pass/fail to results list
        """
        maxiterquery = self.basequery('COUNT(*)', 'run 5 times')
        iters = self.dba.run(maxiterquery)[0][0]
        self.results.append("PASS" if int(iters) == 5 else "FAIL")

    def weekly(self):
        """
        Test weekly.json and append pass/fail to results list
        """
        weekquery = self.basequery('Date', 'run weekly', 'LIMIT 4')
        weeks = self.dba.run(weekquery)
        for w in range(len(weeks) - 1):
            wdiff = weeks[w + 1][0] - weeks[w][0]
            self.results.append("PASS" if "7 days" in str(wdiff) else "FAIL")

    def delay(self):
        """
        Test delay.json and append pass/fail to results list
        """
        delayquery = self.basequery('Date', 'delay')
        delayed = self.dba.run(delayquery)[0][0]
        threeday = date.today() + timedelta(days=3)
        self.results.append("FAIL" if delayed != threeday else "PASS")

    def checkscheduleconfig(self):
        """
        Check the config in scheduled_jobs is correct
        each job has its own check
        

        Returns:
            False if there are any failed tests
            True if all tests PASS
        """
        self.hourly()
        self.iterinmonth()
        self.minutely()
        self.monthly()
        self.repdays()
        self.totaliter()
        self.weekly()
        self.delay()
        return False if "FAIL" in self.results else True

    def checkeditjob(self):
        """
        Run the scheduler script with -e -f to edit a job
        This will edit the job with ID 1
        """
        jobdir = self.dir + '/testfiles/editjson/'
        filename = 'edit.json'
        cmd = '/usr/bin/env python {}/scheduler.py -e {} -f {}{}'.format(self.cwd,
                                                                         '1',
                                                                         jobdir,
                                                                         filename)
        cmd = cmd.split(' ')
        subprocess.Popen(cmd).wait()

        editquery = '''SELECT COUNT(*) FROM {}.{}
        WHERE ID="1"'''.format(self.dba.dbname, self.dba.conftable)
        editresult = self.dba.run(editquery)
        return True if int(editresult[0][0]) else False


def allcheck():
    """
    Run the above checks to make sure the tests ran OK
    append the result to a list
    Check that list for any instance of "False"

    Returns:
        False - if any tests failed
        True - if all tests succeeded
    """
    tr = TestResults()
    results = []
    checktables = tr.checkdbtables()
    print("Table Check: ", "PASS" if checktables else "FAIL")
    results.append(checktables)

    checkconfig = tr.checkconfigname()
    print("Configured Job Check: ", "PASS" if checkconfig else "FAIL")
    results.append(checkconfig)

    checkjob = tr.jobconfigcheck()
    print("Job Config Check: ", "PASS" if checkjob else "FAIL")
    results.append(checkjob)

    checksched = tr.checkscheduled()
    print("Scheduled Check: ", "PASS" if checksched else "FAIL")
    results.append(checksched)

    checkschedconf = tr.checkscheduleconfig()
    print("Schedule Config Check:", "PASS" if checkschedconf else "FAIL")
    results.append(checkschedconf)

    checkedit = tr.checkeditjob()
    print("Editing Job #1:", "PASS" if checkedit else "FAIL")
    results.append(checkedit)

    return False if False in results else True
