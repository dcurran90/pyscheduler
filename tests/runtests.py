import config
import os
import setup
import subprocess
import sys
import results
from logging import *


class AllTests:
    def __init__(self):
        self.admindba = config.setupdb('mysql')
        self.dba = config.setupdb('pysched')
        self.dbname = config.configsectionmap('mysql')['database']
        st = setup.Configure()
        self.cwd = st.setcwd()
        self.dir = self.setdir()

    def setdir(self):
        """
        Sets up the working directory for tests

        Returns:
             working directory
        """
        testdir, filename = os.path.split(os.path.abspath(__file__))
        return testdir

    def createdatabase(self):
        """
        Tell dbactions() what the new database name is
        and create it
        """
        self.admindba.createdb()

    def createuser(self):
        """
        Updates the username to use in dbactions() and then
        creates that user with the relevant permissions
        on the database set up in __init__
        """
        passwd = self.admindba.createusers()
        self.dba.writepasswd()
        self.dba.passwd = passwd

    def createtables(self):
        self.dba.jobconfig()
        self.dba.schedulesjobstable()
        self.dba.completedjobstable()
        self.dba.createagentstable()

    def addjob(self):
        """
        Run the scheduler script with -a -f to add jobs from
        custom JSON file
        """
        jobdir = self.dir + '/testfiles/jobjson/'
        for filename in os.listdir(jobdir):
            cmd = '/usr/bin/env python {}/scheduler.py -a -f {}{}'.format(self.cwd,
                                                                          jobdir,
                                                                          filename)
            cmd = cmd.split(' ')
            subprocess.Popen(cmd).wait()

    def cleanup(self):
        """
        Remove our test database and user
        as they're no longer needed
        """
        dbquery = 'DROP DATABASE {}'.format(self.dbname)
        userquery = '''DELETE FROM {}.{}
        WHERE User="{}"'''.format('mysql',
                                  'user',
                                  self.dba.user)
        self.dba.run(dbquery)
        self.dba.run(userquery)
        self.dba.run('FLUSH PRIVILEGES')


def runall():
    """
    Run all the above functions

    Allows for turning on/off tests by editing
    this secion
    """
    at = AllTests()
    at.createdatabase()
    print("DB created")
    at.createuser()
    print("User created")
    at.createtables()
    print("Tables created")
    at.addjob()
    print("Jobs added")

    print("Checking tests ran OK")
    res = results.allcheck()
    sys.exit(0) if res else sys.exit(1)
