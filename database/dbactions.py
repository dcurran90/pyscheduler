import MySQLdb as mysql
import string
import config
from logging import *
from MySQLdb import OperationalError
from random import sample, choice


class DBSetup:
    """
    All the functions that build the database to the spec
    that is needs to be for the tool to work

    This takes the database architecture out of the hands
    of a sysadmin/developer
    """
    def __init__(self,
                 dbname='',
                 server='',
                 user='',
                 passwd='',
                 scheduser=''):
        """

        Args:
            dbname:
            server:
            user:
            passwd:
            scheduser:
        """
        self.dbname = dbname
        self.server = server
        self.user = user
        self.passwd = passwd
        self.scheduser = scheduser
        self.schedtable = 'scheduled_jobs'
        self.comptable = 'completed_jobs'
        self.conftable = 'jobconfig'
        self.agentstable = 'agentstable'
        self.progresstable = 'inprogress'

    def doall(self):
        """
        Run through all the setup actions
        * create the database
        * create tables
        * create user and set password
        * write the password to pysched section of config.ini
        * flush privileges
        """
        self.createdb()
        self.schedulesjobstable()
        self.completedjobstable()
        self.jobconfig()
        self.createagentstable()
        self.createinprogresstable()
        self.writepasswd()
        # create the user for connecting to the pyscheduler db
        # runs last so that all privileges are updated correctly
        query = "FLUSH PRIVILEGES"
        self.run(query)

    def connect(self):
        """
        Set up the database connection

        Returns:
            connection as an object
        """
        try:
            dbconnect = mysql.connect(host=self.server,
                                      user=self.user,
                                      passwd=self.passwd)
            return dbconnect
        except Exception:
            error("Could not connect to database", exc_info=True)

    def run(self, query):
        """
        Run queries

        Args:
            query: Query to run

        Returns:
            result of database query as fetchall() object
        """
        db = self.connect()
        cur = db.cursor()
        try:
            cur.execute(query)
            if 'INSERT' in query or 'DELETE' in query or 'UPDATE' in query:
                db.commit()
            outcome = cur.fetchall()
            return outcome
        except OperationalError as oe:
            debug("Operational Error: %s" % oe)
            if 'Deadlock found' in oe.message:
                self.run(query)
        except Exception as e:
            warn("Couldn't run query", exc_info=True)
        db.close()

    def createusers(self):
        """
        Create the pysched database user

        Returns:
            generated password for the user
        """
        chars = string.letters + string.digits
        length = 15
        password = ''.join(choice(chars) for _ in range(length))
        connserver = 'localhost'
        query = "GRANT ALL ON {}.* to {}@{} IDENTIFIED BY '{}'".format(self.dbname,
                                                                       self.scheduser,
                                                                       connserver,
                                                                       password,)
        self.run(query)
        return password

    def writepasswd(self):
        if 'password' in config.configsectionmap('pysched'):
            warn("Database user already exists")
            pyuser = config.configsectionmap('pysched')['password']
        else:
            pyuser = self.createusers()
            config.writetoini('[pysched]', 'password', pyuser)
            # write the new user password to the .ini file
        print("Database access details: user = {} password = {}".format(
            self.scheduser,
            pyuser, ))

    def createdb(self):
        """
        Create the database
        """
        query = 'CREATE DATABASE IF NOT EXISTS {}'.format(self.dbname)
        self.run(query)

    def jobconfig(self):
        """
        Create jobconfig table
        """
        query = '''CREATE TABLE IF NOT EXISTS {}.{} (
        ID int NOT NULL AUTO_INCREMENT,
        Command text NOT NULL,
        JobName VARCHAR(180) NOT NULL,
        Hour varchar(2) NOT NULL,
        Minute varchar(2) NOT NULL,
        RepMinutes varchar(2) NOT NULL,
        RepHours varchar(2) NOT NULL,
        RepDays varchar(2) NOT NULL,
        RepWeeks varchar(2) NOT NULL,
        RepMonths varchar(2) NOT NULL,
        DayofWeek varchar(1) NOT NULL,
        IterinMonth varchar(1) NOT NULL,
        EndHour varchar(2) NOT NULL DEFAULT '0',
        EndMinute varchar(2) NOT NULL DEFAULT '0',
        MaxIter varchar(10) NOT NULL DEFAULT '0',
        DelayStart varchar(10) NOT NULL DEFAULT '0',
        Groups varchar(25) NOT NULL DEFAULT 'General',
        PRIMARY KEY (ID),
        UNIQUE KEY ix_jobname (JobName))'''.format(self.dbname, self.conftable,)

        self.run(query)

    def schedulesjobstable(self):
        """
        Create scheduled_jobs table
        """
        query = '''CREATE TABLE IF NOT EXISTS {}.{} (
        ID int NOT NULL AUTO_INCREMENT,
        JobName VARCHAR(255) NOT NULL,
        Time time NOT NULL,
        Date date NOT NULL,
        Command text NOT NULL,
        JobStatus int NOT NULL DEFAULT 1,
        Groups varchar(25) NOT NULL DEFAULT 'general',
        PRIMARY KEY (ID))'''.format(self.dbname, self.schedtable)

        self.run(query)

    def completedjobstable(self):
        """
        Create completed_jobs table
        """
        query = '''CREATE TABLE IF NOT EXISTS {}.{} (
        ID int NOT NULL AUTO_INCREMENT,
        JobID INT NOT NULL,
        JobName VARCHAR(255) NOT NULL,
        Time time NOT NULL,
        Date date NOT NULL,
        ExitStatus int,
        Agent VARCHAR(15) NOT NULL DEFAULT "manual",
        PRIMARY KEY (ID))'''.format(self.dbname, self.comptable,)

        self.run(query)

    def createagentstable(self):
        """
        Create agentstable table
        """
        query = '''CREATE TABLE IF NOT EXISTS {}.{} (
        Address VARCHAR(15) NOT NULL,
        Pubkey text NOT NULL,
        Token VARCHAR(16),
        Status int NOT NULL DEFAULT 1,
        Groups varchar(255) NOT NULL DEFAULT "general",
        PRIMARY KEY (Address))'''.format(self.dbname, self.agentstable)

        self.run(query)

    def createinprogresstable(self):
        """
        Create inprogress table
        """
        query = '''CREATE TABLE IF NOT EXISTS {}.{} (
        ID INT NOT NULL,
        JobName VARCHAR(255) NOT NULL,
        Date date NOT NULL,
        Time time NOT NULL,
        Agent VARCHAR(15) NOT NULL,
        PRIMARY KEY(ID))'''.format(self.dbname, self.progresstable)

        self.run(query)

    def makecomplete(self, jobid, agent, exitcode=0):
        """
        removes the completed job (failed or succeeded)
        from the scheduled_jobs or inprogress table and adds it to the
        completed_jobs table with information about the exit
        status of the script

        which table depends on what the contents of agent is
        if 'manual' then the job was never in progress so
        should be removed from scheduled_jobs

        Args:
            jobid: ID of the job
            agent: the IP address of the agent the job ran on
            exitcode: exitcode from subprocess in runcmd()
        """
        deltable = self.progresstable if agent != 'manual' else self.schedtable
        jobinfoquery = '''SELECT JobName, Time, Date
                FROM {}.{}
                WHERE ID = "{}"'''.format(self.dbname,
                                          deltable,
                                          jobid)
        jobinfo = self.run(jobinfoquery)[0]

        compquery = '''INSERT INTO {}.{}
                (JobID, JobName, Time, Date, ExitStatus, Agent)
                VALUES
                ("{}", "{}", "{}", "{}", "{}", "{}")'''.format(self.dbname,
                                                               self.comptable,
                                                               jobid,
                                                               jobinfo[0],
                                                               jobinfo[1],
                                                               jobinfo[2],
                                                               exitcode,
                                                               agent)
        self.run(compquery)

        deljob = '''DELETE FROM {}.{}
                WHERE ID = "{}"'''.format(self.dbname,
                                          deltable,
                                          jobid)
        self.run(deljob)
