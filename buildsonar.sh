#!/usr/bin/env bash

sonarstring="sonar.projectKey=scheduler:gitlab
sonar.projectName=scheduler
sonar.projectVersion=2.0.5
sonar.sources=."

echo "${sonarstring}" > sonar-project.properties
