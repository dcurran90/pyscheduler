import hashlib
import os
import socket
import sys
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random
from database import dbactions
from logging import *
from setup import Configure
if sys.version_info[0] == 2:
    import ConfigParser
else:
    import configparser as ConfigParser


def _version():
    """
    Give the version number of the tool
    Returns:
        current version

    """
    __version__ = "2.0.5"
    return __version__


configpath = '/etc/scheduler'
dirname, filename = os.path.split(os.path.abspath(__file__))
inifile = '%s/config.ini' % dirname
newinifile = '%s/config.ini' % configpath


def configsectionmap(section):
    """
    Reads the config.ini to get configuration details

    Args:
        section: Section of the ini to read

    Returns:
        dictionary containing the settings
    """
    cfg = ConfigParser.ConfigParser()
    cfg.read(newinifile)
    mydict = {}
    options = cfg.options(section)
    for option in options:
        try:
            mydict[option]= cfg.get(section, option)
            if mydict[option] == -1:
                print("skip: %s" % option)
        except Exception as e:
            print("Exception on %s: %s!" % (option, e))

    return mydict


def setupdb(usertype):
    """
    Database connection details

    Returns:
        connection as an object
    """
    dbinfo = configsectionmap('mysql')
    pysched = configsectionmap('pysched')
    if usertype == 'mysql':
        username = dbinfo['username']
        password = dbinfo['password']
    else:
        username = pysched['username'] if 'password' in pysched else dbinfo['username']
        password = pysched['password'] if 'password' in pysched else dbinfo['password']

    dba = dbactions.DBSetup(server=dbinfo['server'],
                            dbname=dbinfo['database'],
                            user=username,
                            passwd=password,
                            scheduser=pysched['username'])
    return dba


def writetoini(search, key, value):
    """
    Write to config.ini

    Args:
        search: What to search for in the file
        key: What key to add
        value: Value to give the new key
    """
    with open(newinifile, "r") as in_file:
        buf = in_file.readlines()
    with open(newinifile, "w") as out_file:
        for line in buf:
            if line == "{}\n".format(search):
                line = line + "{}: {}\n".format(key, value)
            out_file.write(line)


def givekey(master):
    """
    Send the agents generated keyfile to the master server
    This function goes through the steps:
    * read key from file (set in config.ini)
    * send the key unencrypted to the master server
    * receive an encrypted token and check it matches
    token setting in config.ini
    * send back "OK" if it does match
    * save token to a file for future authentication

    Args:
        master: IP of the master server, taken from sys.argv[0]
    """
    tokenfile = '/etc/scheduler/scheduler.tokens'
    if not os.path.exists(tokenfile):
        os.mknod(tokenfile)

    keyfile = configsectionmap('agent')['rsalocation']
    if not os.path.isfile(keyfile):
        debug("Private key file doesn't exist, creating")
        rgen = Random.new().read
        privkey = RSA.generate(2048, rgen)
        with open(keyfile, 'w') as kfw:
            kfw.write(privkey.exportKey().decode())
        info('Private key file saved at %s' % keyfile)
    with open(keyfile, 'r') as kf:
        debug('Reading key file')
        key = kf.read()
        loadpriv = RSA.importKey(key)
        decryptor = PKCS1_OAEP.new(loadpriv)
        pubkey = loadpriv.publickey().exportKey().decode()
        debug('Key file read and saved')
    ksock = socket.socket()
    try:
        debug('Connecting to %s' % master)
        ksock.connect((master, int(configsectionmap('agent')['masterport'])))
        debug('Connected, now sending public key')
        ksock.sendall("public_key=" + pubkey)
        info('Key exchange complete')
    except Exception as e:
        error("Couldn't send key to master:\n%s" % e)
    enctoken = ksock.recv(2048).replace('token=', '').strip()
    debug('Known token received')
    hashedtoken = decryptor.decrypt(enctoken)
    if hashedtoken == hashtoken():
        agentgroups = configsectionmap('agent')['groups'] \
            if configsectionmap('agent')['groups'] \
            else 'general'
        debug('Received token matches known token, sending groups as confirmation')
        ksock.sendall(agentgroups)
        masterenctoken = ksock.recv(4096).replace('token=', '').strip()
        if "FAIL" not in masterenctoken:
            info('Token handover complete')
            mastertoken = decryptor.decrypt(masterenctoken)
            with open(tokenfile, 'r+') as tfr:
                for line in tfr.readlines():
                    if master in line:
                        break
                else:
                    with open(tokenfile, 'a') as st:
                        st.write("%s %s\n" % (master, mastertoken,))
                        debug('Token %s saved to %s' % (masterenctoken, tokenfile,))
        else:
            ksock.sendall("Registration Failed")
            error('Registration FAILED - %s not recognised' % master)
    else:
        error('Registration FAILED - unknown token received')
        newtoken = 'FAIL'
        ksock.sendall(newtoken)
    ksock.close()


def hashtoken(*args):
    """
    Has a token with a known salt using SHA512 algorithm
    """
    salt = configsectionmap('agent')['salt']
    token = configsectionmap('agent')['token']
    if args:
        token = args[0]
    hashedtoken = hashlib.sha512(token + salt).hexdigest()
    return hashedtoken


"""
Set logging details.
First part sets the logfile to log to
Second part sets up output to the screen
"""
loglevel = INFO
if os.path.isfile(newinifile):
    # set logging to file and output to screen
    logdir = configsectionmap('general')['logdir']
    logfile = configsectionmap('general')['logfile']
    logfile = "{}/{}".format(logdir, logfile)
    setting = Configure()
    setting.setlogdir()
    loglevel = configsectionmap('general')['loglevel'].upper()
    basicConfig(filename=logfile,
                level=loglevel,
                format='%(asctime)s - %(levelname)s: %(message)s',
                datefmt='%d/%m/%Y %H:%M:%S')

stdlog = getLogger()
stdlog.setLevel(loglevel)
outlog = StreamHandler(sys.stdout)
stdlog.addHandler(outlog)
