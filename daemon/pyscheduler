#!/usr/bin/env python
import atexit
import ConfigParser
import hashlib
import json
import MySQLdb as mysql
import os
import pickle
import socket
import string
import subprocess
import sys
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from datetime import date, datetime, timedelta
from logging import *
from multiprocessing import Process
from random import choice
from time import sleep


def exittasks():
    """
    Run these steps when the process stops
    * write message to log
    """
    info('Shutting down')


class DaemonSettings:
    """
    Sets up the settings to be used in the daemon
    """

    def __init__(self):
        """
        """
        self.configini = '/etc/scheduler/config.ini'
        self.genconf = self.openconfig('general')
        self.myconf = self.openconfig('mysql')
        self.pysconf = self.openconfig('pysched')
        self.agentconf = self.openconfig('agent')
        self.server = self.myconf['server']
        self.dbase = self.myconf['database']
        self.user = self.pysconf['username']
        self.passwd = self.pysconf['password']
        self.schedtable = 'scheduled_jobs'
        self.comptable = 'completed_jobs'
        self.agentstable = 'agentstable'
        self.progresstable = 'inprogress'
        self.logfile = '%s/%s' % (self.genconf['logdir'], self.genconf['daemonlog'],)
        self.loglevel = self.genconf['daemonloglevel'].upper()
        self.port = self.agentconf['port']
        self.masterport = int(self.agentconf['masterport'])
        self.bind = self.agentconf['bind']

    def openconfig(self, section):
        """
        Reads the config.ini to get configuration details

        Args:
            section: Section of the ini to read

        Returns:
            dictionary containing the settings
        """
        if os.path.isfile(self.configini):
            cfg = ConfigParser.ConfigParser()
            cfg.read(self.configini)
            confdict = {}
            options = cfg.options(section)
            for option in options:
                try:
                    confdict[option] = cfg.get(section, option)
                except Exception as e:
                    warn("Exception on %s: %s!" % (option, e))
            return confdict
        error("No ini file found, exiting")
        sys.exit(1)

    def runquery(self, query):
        """
        Runs query in the database as set in __init__()

        Args:
            query: query that needs running,
            should be set in the function that calls runquery()

        Returns:
            fetchall() object with output from the query
        """
        try:
            conn = mysql.connect(host=self.server,
                                 user=self.user,
                                 passwd=self.passwd,
                                 db=self.dbase)
            cur = conn.cursor()
        except Exception as e:
            error("Could not connect to database: %s" % e)
            sys.exit(1)
        try:
            debug('Running query %s' % query)
            cur.execute(query)
            if 'INSERT' in query or 'DELETE' in query:
                debug('Committing INSERT/DELETE')
                conn.commit()
            return cur.fetchall()
        except Exception as e:
            error("Query failed %s : \n %s" % (query, e,))
        finally:
            conn.close()

    def getcwd(self):
        """
        Returns:
            working directory of the tool as listed in config.ini
            this is set on install
        """
        try:
            cwd = self.genconf['working_dir']
            return cwd
        except Exception:
            return ""

    def dbcleanup(self, jobid, agent, exitcode=0):
        """
        Marks the job as complete
        Args:
            jobid: ID of the job
            agent: Agent the job ran on
            exitcode: Exit code reported by the Agent
        """
        clean = "{} {} {}".format(jobid, exitcode, agent)
        subprocess.Popen(['/usr/bin/python',
                          '%s/scheduler.py' % self.getcwd(),
                          '-c',
                          '%s' % clean])

    def listener(self):
        """
        This is the scheduler daemon listener. It attaches to a socket
        and waits for messages from Agents.

        When installing an Agent it connects to this port to carry out
        the key/token transfer.

        During normal operation this is used for returning information
        back from the Agent
        """
        debug('Starting listener')
        r = socket.socket()
        r.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        r.bind((self.bind, self.masterport))
        r.listen(5)
        while True:
            ec, a = r.accept()
            jobagent = a[0]
            data = ec.recv(4096)
            if 'public_key=' in data:
                info('Agent sending public key')
                pubkey = data.replace('public_key=', '').strip()
                key = RSA.importKey(pubkey)
                debug('Public key imported for use')
                encryptor = PKCS1_OAEP.new(key)
                knowntoken = self.hashtoken()
                knownenctoken = encryptor.encrypt(knowntoken)
                debug('Sending hashed & encrypted token')
                ec.send('token=' + str(knownenctoken))
                confirm = ec.recv(1024)
                if confirm != "Registration Failed":
                    agentgroups = confirm
                    info('Token handover successful')
                    chars = string.letters + string.digits
                    length = 8
                    token = ''.join(choice(chars) for _ in range(length))
                    debug('Generating authentication token')
                    enctoken = encryptor.encrypt(token)
                    debug('Sending encrypted token')
                    ec.send("token=" + str(enctoken))
                    query = '''INSERT INTO {}.{} (
                    Address, Pubkey, Token, Groups)
                    VALUES
                    ("{}", "{}", "{}", "{})'''.format(self.dbase, self.agentstable,
                                                      a[0], pubkey, token, agentgroups)
                    debug('Saving details to %s' % self.agentstable)
                    self.runquery(query)
                    info('Restarting service following agent registration')
                    restartcmd = "/bin/systemctl restart pyscheduler.service"
                    subprocess.call(restartcmd.split(' '))
                else:
                    error("Token handover FAILED - tokens didn't match")
                    ec.send("FAIL " + confirm)
            else:
                returnmsg = pickle.loads(data)
                jid = returnmsg[0]
                ecode = returnmsg[1]
                if int(ecode) != 0:
                    error("%s failed with exitcode %s" % (jid, ecode))
                self.dbcleanup(jid, jobagent, ecode)
            ec.close()

    def hashtoken(self):
        """
        Has a token with a known salt using SHA512 algorithm
        """
        salt = self.agentconf['salt']
        token = self.agentconf['token']
        hashedtoken = hashlib.sha512(token + salt).hexdigest()
        return hashedtoken

    def inprogress(self, jobid, name, date, time, agent):
        """
        Puts a job in the inprogress table
        removing the job from scheduled_jobs
        """
        progquery = '''INSERT INTO {}.{}
        (ID, JobName, Date, Time, Agent)
        VALUES
        ("{}", "{}", "{}", "{}", "{}")'''.format(self.dbase, self.progresstable,
                                                 jobid, name, date, time, agent)
        self.runquery(progquery)

        delschedquery = '''DELETE FROM {}.{}
        WHERE ID = "{}"'''.format(self.dbase, self.schedtable,
                                  jobid)
        self.runquery(delschedquery)

    def checkagents(self):
        """
        Gets information about known Agents so that
        messages can be encrypted/authenticated
        with the right key/token combination.

        Returns:
            Dictionary containing Agent information:
            * Address
            * Public key for encryption
            * Token for authentication
        """

        agentdict = {}
        query = '''SELECT Address, Pubkey, Token, Groups from {}.{}
        WHERE Status'''.format(self.dbase, self.agentstable)

        output = self.runquery(query)

        for i in output:
            # test each of the agents. They will turn off if they fail
            cmd = '/usr/bin/python {}/scheduler.py -t {}'.format(cwd,
                                                                 str(i[0]).replace('"', ''))
            subprocess.call(cmd.split(' '))
            agentdict[i[0]] = {'key': i[1],
                               'token': i[2],
                               'groups': i[3]}
        return agentdict

    def createcache(self, days):
        """
        Takes information from scheduled_jobs and saves this
        to a dictionary. This dictionary will be used as the cache

        dict structure:
        {"id":{
          "JobName": "jobname",
          "JobTime": "10:30"
          },
        ...
        }

        :param days:
            how many days into the future to get the information
            0 - today
            1 - tomorrow
            etc
            should be 0 on start to get jobs for this day

        :return:
            cachedict object
        """
        queryday = date.today() + timedelta(days=days)
        query = '''SELECT ID, JobName, Time
        FROM {}.{}
        WHERE Date = "{}"
        '''.format(self.dbase, self.schedtable, queryday)
        result = self.runquery(query)
        cachedict = {}
        for j in result:
            # we need to make sure we keep the leading 0 on all times
            # as str strips it off
            jt = str(j[2])
            if len(jt) < 8:
                jt = "0" + jt
            cachedict[int(j[0])] = {'JobName': j[1],
                                    'JobTime': jt}
        with open('/etc/scheduler/cache.json', 'w') as cj:
            cj.write(json.dumps(cachedict, indent=4))
        info('Jobs cached:\n%s' % cachedict)
        return cachedict

    def selectagent(self, agents, group):
        allagents = sorted(agents, key=agents.get)
        agent = ""
        for a in allagents:
            agentgroups = agents[a]['groups'].split(', ')
            if group in agentgroups:
                agent = a
                break

        return agent


if __name__ == "__main__":
    """
    This is the section that acts as the daemon
    it loops infinitely, or until stopped.

    A cache of jobs to run in a day is created on start
    and refreshed every day at 00:00.
    This cache is read every 60 seconds and a check is performed
    to see if the job needs to run.

    If it does then the database is checked to confirm:
    * the command
    * the job is not disabled (JobStatus = 1)

    The command is run in multiprocessing so that multiple jobs can run
    at the same time if necessary.
    """
    atexit.register(exittasks)
    ds = DaemonSettings()
    basicConfig(filename=ds.logfile,
                level=ds.loglevel,
                format='%(asctime)s - %(levelname)s: %(message)s',
                datefmt='%d/%m/%Y %H:%M:%S')
    cache = ds.createcache(0)
    cwd = ds.getcwd()
    if not cwd:
        error('working directory empty, please check config.ini')
        raise ValueError('Working directory not set')

    # create the dictionary of agents and set all counters to 0
    agentlist = ds.checkagents()
    agents = {}
    for i in agentlist:
        agents[i] = 0

    Process(target=ds.listener).start()

    while True:
        today = date.today()
        time = datetime.now().strftime("%H:%M")
        targettime = "00:00"
        if today == today.replace(day=1):
            subprocess.Popen(['/usr/bin/python',
                              '%s/scheduler.py' % cwd,
                              '-s'])
        if time == targettime:
            cache = ds.createcache(0)

        for k in cache:
            # loop over the jobs, assigining ID to k
            jobtime = cache[k]['JobTime']
            jobname = cache[k]['JobName']
            sectime = "{}:00".format(time)
            if sectime == jobtime:
                debug("Time is %s, running job %s" % (sectime, jobname,))
                # if the time matches the job run time then run the job
                tablename = 'scheduled_jobs'
                info('Getting information for %s' % jobname)
                query = '''SELECT Command, Groups
                FROM {}.{}
                WHERE ID = {}
                AND JobName = "{}"
                AND Date = "{}"
                AND Time = "{}"
                AND JobStatus'''.format(ds.dbase, tablename,
                                        k,
                                        jobname,
                                        today,
                                        jobtime)
                # get the details of the job to make sure we capture any changes
                jobdetails = ds.runquery(query)[0]
                if jobdetails:
                    listkey = 0
                    info('Running %s' % jobname)
                    cmd = jobdetails[0]
                    group = jobdetails[1]
                    # get the latest agent for round robin and increment its count by 1
                    agent = ds.selectagent(agentlist, group)
                    if not agent:
                        error('No suitable agents found')
                        ds.dbcleanup(k, 'manual', 300)
                    else:
                        count = agents[agent] + 1
                        agents[agent] = count
                        debug('Using agent %s' % agent)
                        ds.inprogress(k, jobname, today, jobtime, agent)
                        # load the public key and use it for encrypting the message
                        key = agentlist[agent]['key']
                        token = agentlist[agent]['token']
                        encryptor = PKCS1_OAEP.new(RSA.importKey(key))
                        debug('Loaded key and token for %s' % agent)
                        sock = socket.socket()
                        msg = [token, k, cmd]
                        encmsg = encryptor.encrypt(pickle.dumps(msg))
                        try:
                            # send the message to the specified agent
                            info('Sending encrypted message to %s' % agent)
                            sock.connect((agent, int(ds.port)))
                            sock.sendall(encmsg)
                        except Exception as e:
                            error("Connection failed - %s" % e)
                        sock.close()
                else:
                    warn('No information found for %s, perhaps it has been disabled' % jobname)
        sleep(60)
