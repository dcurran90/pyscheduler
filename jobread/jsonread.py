import json
import sys
import config
from logging import *


class JobConf:
    """
    Read the JSON file and validate the contents.

    Then transform the contents into something that
    can be input to the database
    """
    def __init__(self, jsonfile=''):
        """

        Args:
            jsonfile: File to read
        """
        self.jsonfile = jsonfile
        self.jsonobj = self.makejson()
        self.acceptlist = ['jobname', 'command',
                           'hour', 'minute', 'endhour', 'endminute',
                           'dayofweek', 'iterinmonth',
                           'repminutes', 'rephours', 'repdays', 'repweeks', 'repmonths',
                           'iterations', 'maxiter', 'delaystart', 'groups']
        self.required = ['jobname', 'command']
        self.dbname = config.configsectionmap('mysql')['database']
        self.dba = config.setupdb('pysched')

    def makejson(self):
        """
        Reads the file in as a JSON object and returns that value

        Returns:
            jsonobj
        """

        if self.jsonfile:
            with open(self.jsonfile, 'r') as jf:
                jsonobj = json.loads(jf.read())
                return jsonobj
        else:
            raise ValueError('No JSON specified, cannot run')

    def verify(self, data):
        """
        Go through the steps to make sure the job config JSON file is valid
        * repmins and iterinmonth are sensible values
        *
        """
        try:
            for i in self.required:
                if i not in data:
                    raise ValueError("Missing key, %s required" % i)
                self.verifyclash(data)
                self.verifylink(data)
        except ValueError:
            error("There was an error in the job config", exc_info=True)
            sys.exit(1)

    def verifyclash(self, data):
        """
        Verify that there are no clashing parameters in the JSON file
        """
        if 'repminutes' in data and not int(60/int(data['repminutes'])) \
           and int(data['repminutes']) < 60:
            warn("%s not divisible by 60, this could cause some issues" % data['repminutes'])
        if 'iterinmonth' in data and int(data['iterinmonth']) > 4:
            warn("There may not be %s iterations in every month" % data['iterinmonth'])
        if 'repdays' in data and 'repweeks' in data:
            raise ValueError("Cannot have repdays and repweeks together")
        if 'repdays' in data and 'repmonths' in data:
            raise ValueError("Cannot have repdays and repmonths together")
        if 'repweeks' in data and 'repmonths' in data:
            raise ValueError("Cannot have repmonths and repweeks together")

    def verifylink(self, data):
        """
        Verify that any parameters that rely on others are present
        together
        """
        if 'iterinmonth' in data and 'dayofweek' not in data:
            raise ValueError("Iterinmonth needs day parameter set as well")

    def readjson(self):
        """
        Create dictionary of jobs from the JSON file
        and pass this onto getjobparams()
        """
        for jo in self.jsonobj:
            jobdict = {}
            self.verify(jo)
            for i in self.acceptlist:
                if i in jo:
                    jobdict[i] = jo[i]
            self.getjobparams(jobdict)

    def defaults(self):
        """
        Set defaults to 0

        Returns:
            Dictionary of default values
        """
        paramdict = {}
        for i in self.acceptlist:
            paramdict[i] = 0

        return paramdict

    def getjobparams(self, jobs):
        """
        Change the defaults to those set in the JSON file
        Then insert the values into the jobconfig table

        First checks if a job with that name already exists

        Args:
            jobs: Dictionary from jsonread.py
        """
        paramdict = self.defaults()
        for k in jobs:
            paramdict[k] = jobs[k]

        dba = config.setupdb('pysched')

        checkquery = '''SELECT JobName from {}.{}
        WHERE JobName = "{}"'''.format(self.dbname, self.dba.conftable, paramdict['jobname'])
        select = dba.run(checkquery)
        if select:
            warn("%s already exists" % paramdict['jobname'])
        else:
            paramdict['groups'] = 'general' if paramdict['groups'] == 0 \
                else paramdict['groups']
            query = '''INSERT INTO {}.{}
            (JobName, Command, Hour, Minute,
            RepMinutes, RepHours, RepDays, RepWeeks, RepMonths,
            DayofWeek, IterinMonth, EndHour, EndMinute, MaxIter, DelayStart, Groups) VALUES
            ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}",
            "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}")
            '''.format(self.dbname, self.dba.conftable,
                       paramdict['jobname'],
                       paramdict['command'],
                       paramdict['hour'],
                       paramdict['minute'],
                       paramdict['repminutes'],
                       paramdict['rephours'],
                       paramdict['repdays'],
                       paramdict['repweeks'],
                       paramdict['repmonths'],
                       paramdict['dayofweek'],
                       paramdict['iterinmonth'],
                       paramdict['endhour'],
                       paramdict['endminute'],
                       paramdict['maxiter'],
                       paramdict['delaystart'],
                       paramdict['groups'])
            dba.run(query)
        return paramdict
