Job Scheduler written in Python

Requirements
============

* A database service (Only tested with Mariadb 10.0)
* A db user with the privileges to create and grant privileges
* Python (2.7/3.5)
* python-mysqldb  

```bash
apt install python-mysqldb
```

* Everything in requirements.txt:  

```bash
pip install -r requirements.txt
``` 
* 1 CPU
* 256MB RAM (256MB + Job requirements on agents)

Installation
============

```bash
git clone https://git.irt-prod.com/product-engineering/scheduler.git
```

Edit the config.ini file to match your desired details then run

```bash
python setup.py
```

This will run through:
* copying the ini file to /etc/scheduler/config.ini
  creating the directory if it doesn't exists
* Creating the database
* Creating required tables
* Adding a user for the new database
* Adding the new user password to config.ini
* Moving daemon files into the correct place (/usr/bin and /etc/systemd/system)
* Adding the tool working directory to config.ini
* Installing the Django based web gui

Installation of the Agent is also simple. It takes two arguments, "agent" and  
the IP of the Scheduler server to register with.
```bash
python setup.py agent 192.168.0.12
```
The details of exactly what happens are available in the [wiki](https://git.irt-prod.com/product-engineering/scheduler/wikis/Scheduler-agent)

Running commands
================

Commands can be run from the command line and a full list is available in the  
[wiki](https://git.irt-prod.com/product-engineering/scheduler/wikis/home)


Help Output
===========

```
optional arguments:
  -h, --help            show this help message and exit
  --version             Show the current version number
  -l, --list            Show a list of all configured jobs
                                add -m to view registered agents
                                -v is used to show more output
                                -f is used to show output for specific job or agent
  -a, --addjob          Add jobs from jobconfig
  -s, --schedule        Only renew schedule, do not add a job
  -d DELETEJOB, --deletejob DELETEJOB
                        Delete jobs from jobconfig, either by ID or JobName
  -e EDITJOB, --editjob EDITJOB
                        Edit a job by ID with contents of JSON file (must use -f)
                                optionally use -v to also update jobs to new configuration
  -f FUNCTION, --function FUNCTION
                        Extra information to pass to an argument such as file path or ID
  -c COMPLETE, --complete COMPLETE
                        Mark job ID as complete
  -o ON, --on ON        Turn on a job or schedule
  -O OFF, --off OFF     Turn off a job or schedule
  -v, --verbose         Display more information from the command
  --runtests            Run tests
  -m, --modifier        Used to add more functionality to a flag where others don't cut it
```