import config
from jobread import jsonread
import getschedule


class EditJob:
    def __init__(self, jobid=0,
                 jsonfile="",
                 fullupdate=0
                 ):
        self.jobid = jobid
        self.jsonfile = jsonfile
        self.fullupdate = fullupdate
        self.dba = config.setupdb('pysched')

    def edit(self, jobdict=''):
        """
        Edit the jobconfig table for a specific job ID
        """

        jr = jsonread.JobConf(jsonfile=self.jsonfile) if self.jsonfile else ''
        jobj = jr.jsonobj[0] if not jobdict else jobdict

        for col in jobj:
            val = jobj[col]
            editquery = """UPDATE {}.{}
            SET {} = '{}'
            WHERE ID = '{}'""".format(self.dba.dbname,
                                      self.dba.conftable,
                                      col, val, self.jobid
                                      )
            self.dba.run(editquery)
        if self.fullupdate:
            self.schedupdate()

    def schedupdate(self):
        """
        Update the schedule by first deleting the current schedule
        then running the schedulejobs() function from getschedule
        """
        deletequery = """DELETE FROM {}.{}
        WHERE JobName in
        (SELECT JobName from {}.{}
        WHERE ID = '{}')""".format(self.dba.dbname, self.dba.schedtable,
                                   self.dba.dbname, self.dba.conftable,
                                   self.jobid)
        self.dba.run(deletequery)

        js = getschedule.JobSchedule()
        js.schedulejobs()
