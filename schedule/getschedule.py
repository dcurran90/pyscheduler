import config
import datetime
from datetime import datetime, date, timedelta
from dateutil.relativedelta import *
from dateutil.rrule import *
from logging import *


class JobSchedule:
    """
    """
    def __init__(self):
        """
        Set the schedule for a job
        """
        self.dba = config.setupdb('pysched')
        self.dbname = config.configsectionmap('mysql')['database']
        self.now = date.today()
        self.rulestring = "RRULE:"

    def schedulejobs(self):
        """
        Entry point to this class as run from scheduler.py
        """
        self.schedjob()

    def getjobs(self):
        """
        Run query to get the jobs from jobconfig table

        Returns:
            jobs as an object of fetchall()
        """

        query = "SELECT * FROM {}.{}".format(self.dbname, self.dba.conftable)

        jobs = self.dba.run(query)
        return jobs

    def getruledict(self, jobdict):
        """
        Create a dictionary of dates/times to run the jobs

        This essentially turns our settings in config.json into
        parameters that rrule can understand and interpret
        as a dictionary.

        this dictionary is turned into a list and that list into a string
        by calling getjobdate()

        Args:
            jobdict: Dictionary of the job configuration

        Returns:
            The result of getjobdate()
        """
        jobname = jobdict[2]
        debug('Scheduling %s' % jobname)
        ruledict = {'freq': 'DAILY'}

        hour = int(jobdict[3])
        endhour = int(jobdict[12])
        endminute = int(jobdict[13])

        repminutes = int(jobdict[5])
        rephours = int(jobdict[6])
        repdays = int(jobdict[7])
        repweeks = int(jobdict[8])
        repmonths = int(jobdict[9])

        dayofweek = daymap(jobdict[10])
        iterinmonth = int(jobdict[11])

        maxiter = int(jobdict[14])
        delaystart = int(jobdict[15])

        jobtime = datetime.strptime("%s:%s:00" % (hour, jobdict[4],), "%H:%M:%S").time()

        lasttime = "23:59:59"

        # populate the rule dictionary
        if dayofweek:
            ruledict['freq'] = 'WEEKLY'
            ruledict['byweekday'] = dayofweek
        if repminutes:
            ruledict['freq'] = 'MINUTELY'
            ruledict['interval'] = repminutes
        elif rephours:
            ruledict['freq'] = 'HOURLY'
            ruledict['interval'] = rephours
        elif repdays:
            ruledict['freq'] = 'DAILY'
            ruledict['interval'] = repdays
        elif repweeks:
            ruledict['freq'] = 'WEEKLY'
            ruledict['interval'] = repweeks
        elif repmonths:
            ruledict['freq'] = 'MONTHLY'
            ruledict['interval'] = repmonths
        if iterinmonth:
            ruledict['freq'] = 'MONTHLY'
            ruledict['bysetpos'] = iterinmonth
        if maxiter:
            ruledict['count'] = maxiter
        if endhour:
            lasttime = "{}:{}:00".format(endhour, endminute)

        lastdate = self.getfirstschedule(jobname)

        return self.getjobdate(ruledict, jobtime, lasttime, lastdate, delaystart)

    def getfirstschedule(self, jname):
        """
        Gets the last date the is scheduled to run.
        This makes sure that rescheduling follows
        the same rules as the original schedule

        Also stops scheduling on top of itself

        Args:
            jname: Name of the job

        Returns:
             Date of last scheduled run
        """
        query = '''SELECT Date FROM {}.{}
        WHERE JobName = "{}"
        ORDER BY Date
        LIMIT 1'''.format(self.dbname, self.dba.schedtable, jname)

        lastsched = self.dba.run(query)

        if lastsched:
            lastsched = lastsched[0][0]

        return lastsched

    def getjobdate(self, ruledict, jobtime, lasttime, lastdate, delay):
        """
        turns ruledict into a list and the converts that list
        to a string for use ine rrulestr

        Uses the dateutil rrulestr method to create a list
        of dates and times

        Only adds to the list of the time is between
        first run and last run times to avoid having to create
        logic that only runs jobs between allowed times

        Args:
            ruledict: dictionary of rrule understandable rules
            jobtime: first time the job should run, taken from hour:minute in job config
            lasttime: last time the job should run (defaults to 23:59)
            lastdate: last date the job ran

        Returns:
            list of dates/times
        """
        twomonth = date.today() + relativedelta(months=2)
        start = date.today() + timedelta(days=delay)
        firsttime = jobtime

        if lastdate:
            start = lastdate

        startdayime = datetime.combine(start, jobtime)
        # go through jobdict and append to rulelist if needed
        rulelist = []
        for k, v in ruledict.items():
            if v:
                rulelist.append("%s=%s" % (k, v,))
        rulestring = self.rulestring + ";".join(rulelist)

        rule = rrulestr(rulestring,
                        dtstart=startdayime,
                        )

        # get all the dates for the job for the next two months
        jobdate = rule.between(
            after=startdayime,
            before=datetime.combine(twomonth, datetime.min.time()),
            inc=True
        )
        # Get the dates from jobdate and assign them to a list that we'll return
        jobdatelist = []
        for i in jobdate:
            jobdatetime = str(i).split(' ')
            checktime = datetime.strptime(jobdatetime[1], "%H:%M:%S").time()
            if datetime.strptime(lasttime, "%H:%M:%S").time() >= checktime >= firsttime:
                jobdatelist.append(jobdatetime)
        return jobdatelist

    def getjobdict(self, j):
        """
        Create a dictionary of job settings

        Args:
            j: The settings for a particular job

        Returns:
            dictionary of settings
        """
        jobdict = {}
        for i in range(0, len(j)):
            jobdict[i] = j[i]
        return jobdict

    def schedjob(self):
        """
        Takes care of inputting the data to scheduled_jobs

        Will only insert new jobs for this it checks that
        a job doesn't exist with the same:
        JobName AND Time AND Date
        """
        jobs = self.getjobs()
        for j in jobs:
            jobdict = self.getjobdict(j)
            jobdate = self.getruledict(jobdict)  # YYYY-MM-DD HH:MM:SS

            for d in jobdate:
                jobname = jobdict[2]
                command = jobdict[1]
                jobgroup = jobdict[16]
                jobdate = d[0]
                jobtime = d[1]
                debug("Scheduling %s for %s, %s" % (jobname, jobdate, jobtime,))
                debug("To run command %s" % command)
                # query that will only insert if the exact same record doesn't exist already
                query = '''INSERT INTO {}.{}
                (JobName, Time, Date, Command, Groups)
                SELECT * FROM(SELECT "{}", "{}", "{}", "{}", "{}") as tmp
                WHERE NOT EXISTS (
                SELECT * FROM {}.{}
                WHERE JobName="{}"
                AND Time="{}"
                AND Date="{}")LIMIT 1'''.format(self.dbname, self.dba.schedtable,
                                                jobname,
                                                jobtime,
                                                jobdate,
                                                command,
                                                jobgroup,
                                                self.dbname,
                                                self.dba.schedtable,
                                                jobname,
                                                jobtime,
                                                jobdate
                                                )

                self.dba.run(query)


def daymap(day):
    """
    Convert number to string for use in rrulestr

    Args:
        day: The day of week as a str number

    str for day (0-7)

    Returns:
        str for day (MO-SU)
        int if day = '0' so that dayofweek is ignored
    """
    dmap = {'0': 0,
            '1': 'MO',
            '2': 'TU',
            '3': 'WE',
            '4': 'TH',
            '5': 'FR',
            '6': 'SA',
            '7': 'SU'}
    return dmap[day]
