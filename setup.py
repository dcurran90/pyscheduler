import config
import fileinput
import os
import subprocess
import sys
from logging import *


class Configure:
    """
    """
    def __init__(self):
        """
        Steps to configure the scheduler tool
        """
        self.systemd = '/etc/systemd/system/'

    def ver(self):
        """
        Get the version information from config.py

        Returns:
            version number
        """
        version = config._version()
        return version

    def desc(self):
        """
        Returns:
            Simple description of the tool
        """
        desc = "A python based job scheduler"
        return desc

    def updatesonar(self, wd):
        """
        Write the latest version to the sonar-project.properties
        file so that it display correctly in sonarqube
        """
        sonarfile = "{}/{}".format(wd, 'sonar-project.properties')
        if os.path.isfile(sonarfile):
            search = "sonar.projectVersion="
            replace = "{}{}".format(search, self.ver())
            for line in fileinput.input(sonarfile, inplace=True):
                if search in line:
                    print(replace)
                else:
                    print(line.strip('\n\r'))

    def moveconfig(self):
        """
        Put the config.ini into a standard place
        """
        if not os.path.isdir(config.configpath):
            try:
                os.mkdir(config.configpath, 0o644)
            except Exception as e:
                print("Could not make directory: %s" % e)
        if os.path.isfile(config.inifile):
            if os.path.isfile(config.newinifile):
                print("%s already exists, not overwriting" % config.newinifile)
            else:
                print("Moving config.ini to %s" % config.newinifile)
                os.rename(config.inifile, config.newinifile)

    def movedaemon(self, agent=False):
        """
        Put pyscheduler.py into /usr/bin/
        Put pyscheduler.service into /etc/systemd/system
        """
        localdir = config.dirname + '/daemon/'
        daemondir = '/usr/bin/'

        daemonname = 'pyscheduler'
        agentname = 'pyscheduler-agent'
        daemonfile = localdir + daemonname
        daemoninstalled = daemondir + daemonname
        agentinstalled = daemondir + agentname
        servicename = 'pyscheduler.service'
        servicefile = localdir + servicename
        serviceinstalled = self.systemd + servicename
        agentservicename = 'pyscheduler-agent.service'
        agentserviceinstalled = self.systemd + agentservicename

        if agent:
            localdir = config.dirname + '/agent/'
            daemonname = agentname
            daemonfile = localdir + agentname
            daemoninstalled = agentinstalled
            servicefile = localdir + agentservicename
            serviceinstalled = agentserviceinstalled

        if os.path.isdir(daemondir):
            # put the daemon file in place
            try:
                os.rename(daemonfile, daemoninstalled)#
            except Exception:
                if not os.path.isfile(daemoninstalled):
                    raise ValueError('%s Does not exist and has not been moved' % daemonfile)
        else:
            # if /usr/bin doesn't exist error as this probably isn't a Linux system
            raise ValueError('%s does not exist, is this a Linux sytstem?' % daemondir)
        if os.path.isdir(self.systemd):
            # put the service file into place and reload systemctl so systemd can see it
            try:
                os.rename(servicefile, serviceinstalled)
                subprocess.Popen(['/bin/systemctl', 'daemon-reload'])
            except Exception:
                if not os.path.isfile(serviceinstalled):
                    raise ValueError('%s Does not exist and has not been moved' % serviceinstalled)
        else:
            raise ValueError('%s does not exist, is this a systemd system?' % self.systemd)
        # set permissions to rwxr_xr_x
        os.chmod(daemoninstalled, 0o755)
        os.chmod(serviceinstalled, 0o755)
        print('Daemon installed, start with\n\tsystemctl start %s' % daemonname)

    def installwebserver(self):
        webservicename = 'pyscheduler-web.service'
        webservicefile = config.dirname + '/webserver/' + webservicename
        webserviceinstalled = self.systemd + webservicename

        try:
            print("Installing web interface")
            workdir = config.configsectionmap('general')['working_dir']
            bind = config.configsectionmap('agent')['bind']
            user = config.configsectionmap('pysched')['username']
            passwd = config.configsectionmap('pysched')['password']
            pyth = '/usr/bin/python3'
            managepy = '{}/scheduler-django/manage.py'.format(workdir)
            with open(webservicefile, "r") as in_file:
                buf = in_file.readlines()
            with open(webservicefile, "w") as out_file:
                for line in buf:
                    if line == "[Service]\n":
                        line = line + "ExecStart={} {} runserver {}:80\n". \
                            format(pyth, managepy, bind)
                    out_file.write(line)

            os.rename(webservicefile, webserviceinstalled)
            print("Web interface installed, run with\n\tsystemctl start pyscheduler-web")
            os.chmod(webserviceinstalled, 0o755)
            subprocess.call([pyth,
                             managepy,
                             'makemigrations',
                             'scheduler'])
            subprocess.call([pyth,
                             managepy,
                             'sqlmigrate',
                             'scheduler',
                             '0001'])
            subprocess.call([pyth,
                             managepy,
                             'migrate',
                             'scheduler'])
            subprocess.call([pyth,
                             managepy,
                             'migrate'])
            superusercmd = ("from django.contrib.auth.models import User; "
                            "User.objects.create_superuser('{}', 'root@localhost', '{}')").\
                format(user, passwd)
            subprocess.call([pyth,
                             managepy,
                             'shell',
                             '-c',
                             superusercmd])
        except Exception as e:
            raise ValueError("Error installing webserver:\n\t%s" % e)

    def movelogrotate(self):
        rotatefile = config.dirname + '/maintenance/logs/pyscheduler'
        rotatedir = '/etc/logrotate.d/pyscheduler'
        if os.path.isfile(rotatefile):
            try:
                os.rename(rotatefile, rotatedir)
            except Exception as e:
                raise ValueError('Error setting up logrotae: %s' % e)

    def setlogdir(self):
        """
        create log directory if it does not exist
        """
        logdir = config.configsectionmap('general')['logdir']
        if not os.path.isdir(logdir):
            os.mkdir(logdir)

    def setcwd(self):
        """
        Set the working directory in config.ini
        this will be used by the daemon to run commands
        with scheduler.py
        """
        if 'working_dir' not in config.configsectionmap('general'):
            cwd, filename = os.path.split(os.path.abspath(__file__))
            config.writetoini('[general]', 'working_dir', cwd)
            return cwd
        else:
            return config.configsectionmap('general')['working_dir']

    def dbsetup(self):
        """
        Create the database schema
        """
        config.setupdb('mysql').doall()


if __name__ == "__main__":
    """
    Runs through steps to install the tool
    """
    conf = Configure()
    conf.moveconfig()
    conf.setlogdir()
    conf.movelogrotate()
    wd = conf.setcwd()
    if 'agent' in sys.argv:
        info("Installing pyscheduler agent version %s" % conf.ver())
        conf.movedaemon(agent=True)
        config.givekey(sys.argv[2])
    else:
        info("Installing pyscheduler version %s" % conf.ver())
        print(conf.desc())
        conf.movedaemon()
        conf.dbsetup()
        conf.updatesonar(wd)
        conf.installwebserver()
