from __future__ import print_function
import argparse
import config
import os
import socket
import subprocess
import sys
from jobread import jsonread
from logging import *
from schedule import getschedule, editjobs
from tests import runtests


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument(
        "--version",
        help="Show the current version number",
        action="store_true"
    )
    parser.add_argument(
        "-l", "--list",
        help="""Show a list of all configured jobs
        add -m to view registered agents
        -v is used to show more output
        -f is used to show output for specific job or agent""",
        action="store_true"
    )
    parser.add_argument(
        "-a", "--addjob",
        help="Add jobs from jobconfig",
        action="store_true"
    )
    parser.add_argument(
        "-s", "--schedule",
        help="Only renew schedule, do not add a job",
        action="store_true",
    )
    parser.add_argument(
        "-d", "--deletejob",
        help="Delete jobs from jobconfig, either by ID or JobName",
        dest="deletejob",
    )
    parser.add_argument(
        "-e", "--editjob",
        help="""Edit a job by ID with contents of JSON file (must use -f)
        optionally use -v to also update jobs to new configuration""",
        dest="editjob",
    )
    parser.add_argument(
        "-f", "--function",
        help="Extra information to pass to an argument such as file path or ID",
        dest="function",
    )
    parser.add_argument(
        "-c", "--complete",
        help="Mark job ID as complete",
        dest="complete",
    )
    parser.add_argument(
        "-o", "--on",
        help="Turn on a job or schedule",
        dest="on",
    )
    parser.add_argument(
        "-O", "--off",
        help="Turn off a job or schedule",
        dest="off",
    )
    parser.add_argument(
        "-v", "--verbose",
        help="Display more information from the command",
        action="store_true",
    )
    parser.add_argument(
        "--runtests",
        help="Run tests",
        action="store_true",
    )
    parser.add_argument(
        "-m", "--modifier",
        help="Used to add more functionality to a flag where others don't cut it",
        action="store_true",
    )
    parser.add_argument(
        "-t", "--test",
        help="Test if an agent is working",
        dest="agenttest"
    )
    args = parser.parse_args()

    dbname = config.configsectionmap('mysql')['database']
    dba = config.setupdb('pysched')

    if args.version:
        """
        print version then exit
        """
        print(config._version())
        sys.exit(0)

    if args.addjob or args.schedule:
        """
        Add jobs configured in jobconfig, specified in config.ini
        """
        jfile = args.function if args.function else \
            config.configsectionmap('general')['jobconfig']

        if os.path.isfile(jfile) and not args.schedule:
            jc = jsonread.JobConf(jsonfile=jfile)
            jc.readjson()
        elif args.schedule:
            debug("Schedule selected, not checking for JSON file")
        else:
            raise ValueError('%s is not a file' % jfile)
        js = getschedule.JobSchedule()
        js.schedulejobs()

    if args.list:
        """
        List all configured jobs, striping tuple characters
        """
        if args.modifier:
            search = "Address, Status"
            if args.verbose:
                search = search + ", Token, Pubkey, Groups"
            agentquery = '''SELECT {}
            FROM {}.{}'''.format(search, dbname, dba.agentstable)
            if args.function:
                agentquery = agentquery + " WHERE Address = '{}'".format(args.function)
            agent = dba.run(agentquery)
            for a in agent:
                print("AGENTS:\n\t%s : Status - %s" % (a[0],
                                                       "OFF" if int(a[1]) == 0 else "ON"))
                if args.verbose:
                    print("\tGroups: %s\n\tToken: %s" % (a[4], a[2],))
                    print("\tPub Key:\n%s\n\n" % a[3])
        else:
            query = "SELECT ID, JobName, Groups, Command FROM {}.{}".format(dbname, dba.conftable) \
                if args.verbose \
                else "SELECT ID, JobName FROM {}.{}".format(dbname, dba.conftable)
            joblist = dba.run(query)
            for job in joblist:
                print("%s - %s" % (int(job[0]), job[1]))
                if args.verbose:
                    print("\tGroup: %s\n\tCommand: %s" % (job[2],job[3],))
                if args.verbose and args.function and int(args.function) == int(job[0]):
                    verbosequery = '''SELECT Date, Time
                    FROM {}.{}
                    WHERE JobName="{}"'''.format(dbname, dba.schedtable, job[1])
                    times = dba.run(verbosequery)
                    print("\t Next scheduled runs:")
                    timelist = ["%s %s" % (str(i[0]), str(i[1])) for i in times]
                    for t in timelist:
                        print("\t\t%s" % t)

    if args.deletejob and not args.modifier:
        """
        Delete jobs from jobconfig table and optionally scheduled_jobs table
        if just an ID is passed then it is removed from jobconfig
        if a Name is passed then all scheduled jobs will be removed as well
        """
        jobquery = '''DELETE FROM {}.{}
        WHERE
        ID = "{}"
        OR JobName = "{}"'''.format(dbname, dba.conftable,
                                    args.deletejob, args.deletejob)
        info("Deleting job %s" % args.deletejob)
        dba.run(jobquery)

        try:
            int(args.deletejob)
            debug("Not removing from schedule as only ID was passed")
        except ValueError:
            info("Deleting schedule for %s" % args.deletejob)
            schedquery = '''DELETE FROM {}.{}
            WHERE JobName = "{}"'''.format(dbname, dba.schedtable,
                                           args.deletejob)
            dba.run(schedquery)

    if args.deletejob and args.modifier:
        """
        With the modifier flag an agent will be deleted
        It removes agents based on their Address
        """
        info("Delete agent %s from agentstable" % args.deletejob)
        deleteagentquery = '''DELETE FROM {}.{}
        WHERE
        Address = "{}"'''.format(dbname, dba.agentstable,
                                 args.deletejob)
        dba.run(deleteagentquery)

        debug("Restarting daemon")
        restartcmd = "/bin/systemctl restart pyscheduler.service"
        subprocess.call(restartcmd.split(' '))

    if args.on or args.off:
        """
        Change a job from status 1 (on) to status 0(off)
        or vice versa depending on -o or -O
        If just an ID is supplied then a single job will be changed
        if a job name is supplied then all schedules will change
        """
        toggle = "1" if args.on else "0"
        search = args.on if args.on else args.off
        if not args.modifier:
            if args.off:
                tablekey = "ID" if args.off.isdigit() else "JobName"
            else:
                tablekey = "ID" if args.on.isdigit() else "JobName"
            onoffquery = '''UPDATE {}.{}
            SET JobStatus={}
            WHERE {}="{}"'''.format(dbname, dba.schedtable,
                                    toggle,
                                    tablekey, search)
            info("Changing %s to %s" % (search, toggle))
        else:
            onoffquery = '''UPDATE {}.{} 
            SET Status={} 
            WHERE Address="{}"'''.format(dbname, dba.agentstable,
                                       toggle,
                                       str(search))
        dba.run(onoffquery)

    if args.complete:
        """
        Mark a job ID as complete
        by moving it to the completed_jobs table
        """
        compin = args.complete.split(' ')
        jobid = compin[0]
        exitcode = compin[1]
        agent = compin[2] if len(compin) == 3 else 'manual'
        dba.makecomplete(jobid, agent, exitcode)

    if args.runtests and 'SCHEDTESTING' in os.environ:
        """
        Runs tests from under ./tests/
        """
        print("run tests")
        runtests.runall()

    if args.editjob:
        """Edit the configuration of a job
        the settings in a JSON file will be changed in jobconfig table
        optionally reschedule all jobs to pass the updated config
        """
        if not args.function:
            error("Cannot edit without a JSON file please use -f")
            sys.exit(1)
        fullup = 0 if not args.verbose else 1
        ej = editjobs.EditJob(jobid=args.editjob,
                              jsonfile=args.function,
                              fullupdate=fullup)
        ej.edit()

    if args.agenttest:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = config.configsectionmap('agent')['port']
        try:
            agentup = sock.connect((str(args.agenttest), int(port)))
        except Exception as e:
            error('%s not listeining on port %s' % (args.agenttest, port))
            wdir = config.configsectionmap('general')['working_dir']
            cmd = '/usr/bin/python {}/scheduler.py -mO {}'.format(wdir, args.agenttest)
            subprocess.Popen(cmd.split(' '))

