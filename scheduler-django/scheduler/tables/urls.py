from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='tableview'),
    path('table/jobconfig', views.jobconfig, name='jobconfig'),
    path('table/scheduled_jobs', views.scheduled, name='schedjob'),
    path('table/completed_jobs', views.completed, name='compjob'),
    path('table/inprogress', views.inprog, name='inprog'),
    path('delete/<str:table>/<str:jid>', views.delete, name='delete')
]
