import re
from django import forms


class JobSearchForm(forms.Form):
    word = forms.CharField(max_length=255, label='Search for Job Name')

    def clean(self):
        word = self.cleaned_data['word']
        if not re.search(r"^[A-Za-z'0-9 ]+$", word):
            raise forms.ValidationError('Job Name should have letters and numbers only')
        else:
            return self.cleaned_data
