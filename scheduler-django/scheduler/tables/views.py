from ..common import DBconnects
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from . import forms


tablespage = 'tables.html'
db = DBconnects()

@login_required
def index(request):
    return render(request, tablespage, {'plain': True})


def tableget(tname, clause='', order=''):
    tablequery = """SELECT * FROM {} 
    {}
    {}""".format(tname, clause, order)
    tabledata = db.runquery(tablequery)
    return tabledata


@login_required
def jobconfig(request):
    tabledata = tableget('jobconfig')
    return render(request, tablespage, {'jobconfig': tabledata})


@login_required
def scheduled(request):
    tname = 'scheduled_jobs'
    order = "ORDER BY Date, Time"
    tabledata = tableget(tname, order)
    form = forms.JobSearchForm()
    if request.POST:
        form = forms.JobSearchForm(request.POST)
        if form.is_valid():
            clause = "WHERE JobName='{}'".format(form.cleaned_data['word'])
            tabledata = tableget(tname, clause)

    return render(request, tablespage, {'schedjobs': tabledata,
                                        'form': form,
                                        'table': tname})


@login_required
def completed(request):
    tname = 'completed_jobs'
    order = "ORDER BY Date DESC, Time DESC"
    tabledata = tableget(tname, order)
    form = forms.JobSearchForm()
    if request.POST:
        form = forms.JobSearchForm(request.POST)
        if form.is_valid():
            clause = "WHERE JobName='{}'".format(form.cleaned_data['word'])
            tabledata = tableget(tname, clause)
    return render(request, tablespage, {'compjobs': tabledata,
                                        'form': form,
                                        'table': tname})


@login_required
def inprog(request):
    tname = 'inprogress'
    order = "ORDER BY Date DESC, Time DESC"
    tabledata = tableget(tname, order)
    return render(request, tablespage, {'inprog': tabledata,
                                        'table': tname})


@login_required
def delete(request, table, jid):
    deletequery = """DELETE FROM {}
    WHERE ID='{}'""".format(table, jid)
    db.runquery(deletequery)
    return HttpResponseRedirect('/tables/table/%s' % table)
