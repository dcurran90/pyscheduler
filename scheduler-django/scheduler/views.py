import subprocess
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .common import DBconnects, Constants
from .models import *
from .forms import *

c = Constants()


def userlogin(request):
    if request.user.username:
        return HttpResponseRedirect("/")
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')
            form = LoginForm(request.POST)
            error = "Username or Password incorrect"
            return render(request, 'login.html', {'form': form, 'outcome': error})
    form = LoginForm()
    return render(request, 'login.html', {'form': form})


@login_required
def userlogout(request):
    logout(request)
    return HttpResponseRedirect("/")


def index(request):
    db = DBconnects()
    installed = State.objects.values('Installed')
    if not installed:
        form = InstallForm()
        if request.POST:
            form = InstallForm(request.POST)
            if form.is_valid():
                state = State(
                    WorkingDir=form.cleaned_data['workingdir'],
                    DBHost=form.cleaned_data['dbhost'],
                    dbuser=form.cleaned_data['dbuser'],
                    dbpass=form.cleaned_data['dbpass'],
                    dbdatabase=form.cleaned_data['dbdatabase'],
                    logsdir=form.cleaned_data['logsdir']
                )
                state.save()
                return HttpResponseRedirect('')
            else:
                return render(request, 'install.html', {'form': form})
        return render(request, 'install.html', {'form': form})
    else:
        if not request.user.is_authenticated:
            return HttpResponseRedirect("/login/")
        inprogressquery = "SELECT * FROM inprogress"
        inprogress = db.runquery(inprogressquery)
        completequery = """SELECT * FROM completed_jobs
        ORDER BY Date DESC, Time DESC
        LIMIT 10"""
        complete = db.runquery(completequery)

        schedquery = """SELECT * FROM scheduled_jobs
        WHERE JobStatus
        ORDER BY Date, Time
        LIMIT 10"""
        schedjobs = db.runquery(schedquery)

        status = subprocess.Popen(['/bin/systemctl', 'status', 'pyscheduler.service'],
                                  stdout=subprocess.PIPE,)

        pystatus = "No status available"
        for line in status.stdout.readlines():
            if 'Active' in str(line):
                pystatus = str(line).split(' ')
                break
        schedinfo = "{} Started {}, {}".format(pystatus[4],
                                               pystatus[8],
                                               pystatus[9]) if 'inactive' not in pystatus else pystatus[4]
        version = subprocess.Popen(['/usr/bin/python',
                                    '%s/scheduler.py' % c.schedir,
                                    '--version'],
                                    stdout=subprocess.PIPE).communicate()[0].decode("utf-8")
        return render(request, 'index.html', {'inprogress': inprogress,
                                              'complete': complete,
                                              'status': schedinfo,
                                              'version': version,
                                              'future': schedjobs})


def schedrestart(request):
    restartcmd = "/bin/systemctl restart pyscheduler.service"
    subprocess.call(restartcmd.split(' '))
    return HttpResponseRedirect('/')
