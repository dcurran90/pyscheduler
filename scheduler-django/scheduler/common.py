import MySQLdb as mysql
from .models import *


class DBconnects:
    def __init__(self, ):
        self.details = self.getdetails()

    def getdetails(self):
        return State.objects.values_list('DBHost', 'dbuser', 'dbpass', 'dbdatabase')

    def connect(self):
        connection = mysql.connect(host=self.details[0][0],
                                   user=self.details[0][1],
                                   passwd=self.details[0][2],
                                   db=self.details[0][3])
        return connection

    def runquery(self, query):
        db = self.connect()
        cur = db.cursor()

        cur.execute(query)
        if 'INSERT' in query or 'DELETE' in query or 'UPDATE' in query or 'REPLACE' in query:
            db.commit()
            outcome = cur.fetchall()
        else:
            columns = cur.description
            outcome = [{columns[index][0]:column
                       for index, column in enumerate(value)}
                       for value in cur.fetchall()]
        db.close()
        return outcome


class Constants:
    def __init__(self):
        self.pyth = '/usr/bin/python3'
        self.schedir = State.objects.values('WorkingDir')[0]['WorkingDir']

