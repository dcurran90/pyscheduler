from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='jobhome'),
    path('<str:jobname>', views.schedule, name='schedule'),
    path('joboff/<int:jid>', views.joboff, name='delconf'),
    path('jobon/<int:jid>', views.jobon, name='delsched'),
    path('deljob/<str:jid>', views.deljob, name='deljob'),
    path('add/', views.addjob, name='addjob'),
    path('edit/<int:jid>', views.addjob, name='editjob'),
    path('edit/', views.addjob, name='edit')
]
