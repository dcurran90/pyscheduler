from django import forms


class AddJobForm(forms.Form):
    daychoice = (('0', ' '),
                 ('1', 'Monday'),
                 ('2', 'Tuesday'),
                 ('3', 'Wednesday'),
                 ('4', 'Thursday'),
                 ('5', 'Friday'),
                 ('6', 'Saturday'),
                 ('7', 'Sunday'))
    Name = forms.CharField(max_length=255, label="JobName*")
    Command = forms.CharField(label="Command*")
    Hour = forms.CharField(max_length=2, label="Hour*")
    Minute = forms.CharField(max_length=2, label="Minute*")
    EndHour = forms.CharField(max_length=2, empty_value='0', required=False)
    EndMin = forms.CharField(max_length=2, empty_value='0', required=False)
    RepMins = forms.CharField(max_length=2, empty_value='0', required=False)
    RepHours = forms.CharField(max_length=2, empty_value='0', required=False)
    RepDays = forms.CharField(max_length=2, empty_value='0', required=False)
    RepWeeks = forms.CharField(max_length=2, empty_value='0', required=False)
    RepMonths = forms.CharField(max_length=2, empty_value='0', required=False)
    DayofWeek = forms.ChoiceField(choices=daychoice)
    IterinMonth = forms.CharField(max_length=1, empty_value='0', required=False)
    MaxIter = forms.CharField(max_length=10, empty_value='0', required=False)
    DelayStart = forms.CharField(max_length=10, empty_value='0', required=False)
    Group = forms.CharField(max_length=10, empty_value='General', required=False)

    def clean(self):
        # make sure input is a number
        numberfields = ['Hour', 'Minute', 'EndHour', 'EndMin',
                        'RepMins', 'RepHours', 'RepDays', 'RepWeeks', 'RepMonths',
                        'IterinMonth', 'MaxIter', 'DelayStart']
        for nf in numberfields:
            if not self.cleaned_data[nf].isdigit():
                raise forms.ValidationError('%s should be an int' % nf)
        # change double quotes for single quotes
        quotefields = ['Name', 'Command']
        for qf in quotefields:
            if '"' in self.cleaned_data[qf]:
                self.cleaned_data[qf] = self.cleaned_data[qf].replace('"', "'")

        # check for operational clashes
        if self.cleaned_data['RepDays'] != '0' and self.cleaned_data['RepWeeks'] != '0':
            raise forms.ValidationError('Cannot have repdays and repweeks together')
        if self.cleaned_data['RepDays'] != '0' and self.cleaned_data['RepMonths'] != '0':
            raise forms.ValidationError('Cannot have repdays and repmonths together')
        if self.cleaned_data['RepWeeks'] != '0' and self.cleaned_data['RepMonths'] != '0':
            raise forms.ValidationError('Cannot have repmonths and repweeks together')
        if self.cleaned_data['IterinMonth'] != '0' and self.cleaned_data['DayofWeek'] == '0':
            raise forms.ValidationError('Iterinmonth needs a dayofweek setting')

        return self.cleaned_data
