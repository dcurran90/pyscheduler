import subprocess
from ..common import DBconnects, Constants
from ..models import State
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from . import forms

db = DBconnects()
c = Constants()

@login_required
def index(request):

    jobsquery = "SELECT * FROM jobconfig"
    jobs = db.runquery(jobsquery)
    for j in jobs:
        if j['RepMinutes'] != '0':
            j['RepLength'] = j['RepMinutes']
            j['JobType'] = 'Minutes'
        elif j['RepHours'] != '0':
            j['RepLength'] = j['RepHours']
            j['JobType'] = 'Hours'
        elif j['RepDays'] != '0':
            j['RepLength'] = j['RepDays']
            j['JobType'] = 'Days'
        elif j['RepWeeks'] != '0':
            j['RepLength'] = j['RepWeeks']
            j['JobType'] = 'Weeks'
        elif j['RepMonths'] != '0':
            j['RepLength'] = j['RepMonths']
            j['JobType'] = 'Months'
        else:
            j['RepLength'] = '1'
            j['JobType'] = 'Days'

    return render(request, 'jobs.html', {'jobs': jobs})


@login_required
def schedule(request, jobname):
    schedquery = 'SELECT * FROM scheduled_jobs WHERE JobName = "{}"'.format(jobname)
    sched = db.runquery(schedquery)

    return render(request, 'sched.html', {'schedule': sched, 'jn': jobname})


def runscript(option, jid=''):
    cmd = '/usr/bin/python {}/scheduler.py {}'.format(
        c.schedir,
        option).split(' ')
    if jid != '':
        cmd.append(jid)
    subprocess.call(cmd)


@login_required
def joboff(request, jid):
    runscript("-O", str(jid))
    return HttpResponseRedirect('/jobs/')


@login_required
def jobon(request, jid):
    runscript("-o", str(jid))
    return HttpResponseRedirect('/jobs/')


@login_required
def deljob(request, jid):
    runscript("-d", jid)
    return HttpResponseRedirect('/jobs/')


@login_required
def addjob(request, jid=0):
    if jid:
        jinfoquery = """SELECT * FROM jobconfig
        WHERE ID = '{}'""".format(jid)
        jinfo = db.runquery(jinfoquery)[0]
        initial = {
            'Name': jinfo['JobName'],
            'Command': jinfo['Command'],
            'Hour': jinfo['Hour'],
            'Minute': jinfo['Minute'],
            'EndHour': jinfo['EndHour'],
            'EndMin': jinfo['EndMinute'],
            'RepMins': jinfo['RepMinutes'],
            'RepHours': jinfo['RepHours'],
            'RepDays': jinfo['RepDays'],
            'RepWeeks': jinfo['RepWeeks'],
            'RepMonths': jinfo['RepMonths'],
            'DayofWeek': jinfo['DayofWeek'],
            'IterinMonth': jinfo['IterinMonth'],
            'MaxIter': jinfo['MaxIter'],
            'DelayStart': jinfo['DelayStart'],
            'Group': jinfo['Groups']
        }
    form = forms.AddJobForm() if not jid else forms.AddJobForm(initial=initial)
    if request.POST:
        form = forms.AddJobForm(request.POST)
        if form.is_valid():
            addjobquery = """REPLACE INTO jobconfig
                        (JobName, Command, Hour, Minute, EndHour, EndMinute, 
                        RepMinutes, RepHours, RepDays, RepWeeks, RepMonths, 
                        DayofWeek, IterinMonth, MaxIter, DelayStart, Groups)
                        VALUES("{}", "{}", '{}', '{}', '{}', '{}',
                         '{}', '{}', '{}', '{}', '{}',
                         '{}', '{}', '{}', '{}', '{}')""".format(
                form.cleaned_data['Name'],
                form.cleaned_data['Command'],
                form.cleaned_data['Hour'],
                form.cleaned_data['Minute'],
                form.cleaned_data['EndHour'],
                form.cleaned_data['EndMin'],
                form.cleaned_data['RepMins'],
                form.cleaned_data['RepHours'],
                form.cleaned_data['RepDays'],
                form.cleaned_data['RepWeeks'],
                form.cleaned_data['RepMonths'],
                form.cleaned_data['DayofWeek'],
                form.cleaned_data['IterinMonth'],
                form.cleaned_data['MaxIter'],
                form.cleaned_data['DelayStart'],
                form.cleaned_data['Group']
            )
            delquery = """DELETE FROM scheduled_jobs
            WHERE JobName = '{}'""".format(form.cleaned_data['Name'])
            db.runquery(addjobquery)
            db.runquery(delquery)
            runscript("-s")

        return HttpResponseRedirect('/jobs/')

    return render(request, 'addjob.html', {'form': form, 'edit': jid})
