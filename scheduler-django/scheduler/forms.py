import MySQLdb as mysql
import os
from django import forms


class InstallForm(forms.Form):
    workingdir = forms.CharField(label='Scheduler Location')
    dbhost = forms.CharField(label='Scheduler Database Server IP', max_length=15)
    dbuser = forms.CharField(label='DB User', max_length=255)
    dbpass = forms.CharField(label='DB User password', max_length=255, widget=forms.PasswordInput())
    dbdatabase = forms.CharField(label='Database Name',max_length=255)
    logsdir = forms.CharField(label='Logs Directory', max_length=255)

    def clean(self):
        try:
            mysql.connect(host=self.cleaned_data['dbhost'],
                          user=self.cleaned_data['dbuser'],
                          passwd=self.cleaned_data['dbpass'],
                          db=self.cleaned_data['dbdatabase'])
        except Exception:
            raise forms.ValidationError('Database connection details incorrect')
        if not os.path.exists(self.cleaned_data['workingdir']):
            raise forms.ValidationError('%s does not exist' % self.cleaned_data['workingdir'])
        if not os.path.exists(self.cleaned_data['logsdir']):
            raise forms.ValidationError('%s does not exist' % self.cleaned_data['logsdir'])
        return self.cleaned_data


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    def clean(self):
        return self.cleaned_data
