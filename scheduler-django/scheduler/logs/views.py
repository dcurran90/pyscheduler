import os
from ..models import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import render


def getlogdir():
    return State.objects.values('logsdir')[0]['logsdir']


@login_required
def index(request):
    logfiles = os.listdir(getlogdir())
    return render(request, 'logs.html', {'files': logfiles})


@login_required
def readlog(request, logfile):
    with open('%s/%s' % (getlogdir(), logfile), 'r') as lf:
        logcontents = lf.readlines()

    return render(request, 'logs.html', {'log': logcontents})
