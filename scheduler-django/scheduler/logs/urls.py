from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='logsview'),
    path('read/<str:logfile>', views.readlog, name='logsread'),
]
