import subprocess
from ..common import DBconnects, Constants
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

c = Constants()


@login_required
def index(request):
    db = DBconnects()
    agentsquery = "SELECT * FROM agentstable"
    agents = db.runquery(agentsquery)

    return render(request, 'agents.html', {'agents': agents})


@login_required
def toggle(request, agent):
    tog = "-m"
    tog = tog + "O" if agent.startswith('d-') else tog + "o"
    address = agent.replace('d-', '').replace('e-', '')
    cmd = '/usr/bin/python {}/scheduler.py {} {}'.format(c.schedir, tog, address)
    subprocess.call(cmd.split(' '))
    subprocess.Popen(['/bin/systemctl', 'restart', 'pyscheduler.service'])

    return HttpResponseRedirect('/agents/')


@login_required
def delete(request, agent):
    cmd = '/usr/bin/python {}/scheduler.py -md {}'.format(
        c.schedir,
        agent).split(' ')

    subprocess.Popen(cmd)
    return HttpResponseRedirect('/agents/')
