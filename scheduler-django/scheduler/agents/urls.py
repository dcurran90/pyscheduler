from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('toggle/<str:agent>', views.toggle, name='disable'),
    path('delete/<str:agent>', views.delete, name='delete')
]
