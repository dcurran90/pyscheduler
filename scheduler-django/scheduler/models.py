from django.db import models


class State(models.Model):
    Installed = models.BooleanField(default=False)
    WorkingDir = models.TextField()
    Logo = models.TextField()
    DBHost = models.CharField(max_length=15)
    dbuser = models.CharField(max_length=255)
    dbpass = models.CharField(max_length=255)
    dbdatabase = models.CharField(max_length=255)
    logsdir = models.CharField(max_length=255)
